FROM node:14.0.0-stretch
ARG NODE_ENV=development
ENV NODE_ENV=${NODE_ENV}
WORKDIR /app

RUN npm install -g @angular/cli

COPY entrypoint.sh /entrypoint.sh

RUN chmod +x /entrypoint.sh

CMD ["/bin/sh",  "-c",  "envsubst < /usr/share/nginx/html/assets/env.template.json > /usr/share/nginx/html/assets/env.json"]

ENTRYPOINT ["/entrypoint.sh"]
