import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule, APP_INITIALIZER } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { SearchComponent } from './search/search.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { SearchResultComponent } from './search/search-result/search-result.component';
import { SearchResultListComponent } from './search/search-result-list/search-result-list.component';
import { LoadingSpinnerComponent } from './shared/loading-spinner/loading-spinner.component';
import { SearchResultSegmentComponent } from './search/search-result/search-result-segment/search-result-segment.component';
import { OwlDateTimeModule, OwlMomentDateTimeModule } from '@danielmoncada/angular-datetime-picker';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SearchLogoComponent } from './search/search-logo/search-logo.component';
import { SearchSummaryComponent } from './search/search-summary/search-summary.component';
import { TooltipDirective } from './shared/tooltip.directive';
import { ScrollToTopComponent } from './shared/scroll-to-top/scroll-to-top.component';
import { InfoCircleComponent } from './shared/icons/info-circle/info-circle.component';
import { ArrowRightComponent } from './shared/icons/arrow-right/arrow-right.component';
import { ArrowUpSquareComponent } from './shared/icons/arrow-up-square/arrow-right.component';
import { SearchMobileModalComponent } from './search/search-mobile-modal/search-mobile-modal.component';
import { SearchUserIdentityComponent } from './search/search-user-identity/search-user-identity.component';
import { InfoCircleFillComponent } from './shared/icons/info-circle-fill/info-circle-fill.component';
import { EnvironmentHttpService } from './shared/services/environment-http.service';
import { PaxCountDirective } from './search/directives/pax-count.directive';
import { ExclamationCircleFillComponent } from './shared/icons/exclamation-circle-fill/exclamation-circle-fill.component';
import { AppInfoComponent } from './shared/app-info/app-info.component';

export function app_Init(settingsHttpService: EnvironmentHttpService) {
  return () => settingsHttpService.initializeApp();
}

@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    SearchResultComponent,
    SearchResultListComponent,
    LoadingSpinnerComponent,
    SearchResultSegmentComponent,
    SearchLogoComponent,
    SearchSummaryComponent,
    TooltipDirective,
    ScrollToTopComponent,
    InfoCircleComponent,
    ArrowRightComponent,
    ArrowUpSquareComponent,
    SearchMobileModalComponent,
    SearchUserIdentityComponent,
    InfoCircleFillComponent,
    PaxCountDirective,
    ExclamationCircleFillComponent,
    AppInfoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    OwlDateTimeModule,
    OwlMomentDateTimeModule,
    BrowserAnimationsModule
  ],
  providers: [
    {provide: LOCALE_ID, useValue: 'ro'},
    {provide: APP_INITIALIZER, useFactory: app_Init, deps: [EnvironmentHttpService], multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
