import { Component, Input, OnInit } from '@angular/core';
import { SearchResultSegmentModel } from '../../../shared/models/search-result-segment.model';
import { ImagesService } from '../../../shared/services/images.service';

@Component({
  selector: 'app-search-result-segment',
  templateUrl: './search-result-segment.component.html',
  styleUrls: ['./search-result-segment.component.css']
})
export class SearchResultSegmentComponent implements OnInit {
  @Input() segment: SearchResultSegmentModel;
  @Input() dataSource: string;
  @Input() isFirst: boolean;
  @Input() isLast: boolean;
  @Input() way: string;

  constructor(private imagesService: ImagesService) {
  }

  ngOnInit(): void {
  }

  getCompanyImage() {
    return this.imagesService.getCompanyImage(
      this.segment.companyCode,
      this.dataSource,
      this.segment.transportType
    );
  }

  getTransportTypeIcon() {
    return this.dataSource === 'Kiwi-Transport' ?
      this.imagesService.getTransportTypeIcon(
        this.segment.transportType,
        'sm'
      ) :
      '';
  }

  getArrowIcon(direction: string) {
    return this.imagesService.getArrowIcon(direction);
  }
}
