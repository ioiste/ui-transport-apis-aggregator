import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import * as rug from 'random-username-generator';

@Component({
  selector: 'app-search-user-identity',
  templateUrl: './search-user-identity.component.html',
  styleUrls: ['./search-user-identity.component.css']
})
export class SearchUserIdentityComponent implements OnInit {
  @Output() usernameEmitter = new EventEmitter<string>();
  username: string;

  constructor() {
  }

  ngOnInit(): void {
    this.username = this.getUsername();
    this.usernameEmitter.emit(this.username);
  }

  getUsername(): string {
    if (localStorage.getItem('username') === null) {
      this.setUsername();
    }

    return localStorage.getItem('username');
  }

  setUsername() {
    localStorage.setItem('username', rug.generate());
  }
}
