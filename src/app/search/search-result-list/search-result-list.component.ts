import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { SearchResultModel } from '../../shared/models/search-result.model';

@Component({
  selector: 'app-search-result-list',
  templateUrl: './search-result-list.component.html',
  styleUrls: ['./search-result-list.component.css']
})
export class SearchResultListComponent implements OnInit, OnChanges {
  @Input() results: SearchResultModel[];

  constructor() {
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
  }
}
