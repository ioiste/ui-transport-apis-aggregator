import { Directive } from '@angular/core';
import { NG_VALIDATORS, Validator, ValidationErrors, FormGroup } from '@angular/forms';

@Directive({
  selector: '[appPaxCount]',
  providers: [{provide: NG_VALIDATORS, useExisting: PaxCountDirective, multi: true}]
})
export class PaxCountDirective implements Validator {
  constructor() {
  }

  validate(control: FormGroup): ValidationErrors {
    const adultsCount = parseInt(control.get('pax_adults') ? control.get('pax_adults').value : '0', 10);
    const childrenCount = parseInt(control.get('pax_children') ? control.get('pax_children').value : '0', 10);
    const infantsCount = parseInt(control.get('pax_infants') ? control.get('pax_infants').value : '0', 10);
    let isInvalid = false;
    if (
      adultsCount + childrenCount + infantsCount > 9 ||
      adultsCount + childrenCount + infantsCount < 1 ||
      adultsCount < 1 ||
      childrenCount < 0 ||
      infantsCount < 0
    ) {
      isInvalid = true;
    }

    return isInvalid ? {paxCount: true, pax_adults: true, pax_children: true, pax_infants: true} : null;
  }
}
