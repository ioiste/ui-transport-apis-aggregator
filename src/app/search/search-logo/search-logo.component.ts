import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations';

@Component({
  selector: 'app-search-logo',
  animations: [
    trigger('bigSmall', [
      state('big', style({
        marginTop: '-50px',
      })),
      state('small', style({
        marginBottom: '-100px',
        marginTop: '-90px',
      })),
      transition('big => small', [
        animate('1s')
      ]),
      transition('small => big', [
        animate('0.5s')
      ]),
    ]),
  ],
  templateUrl: 'search-logo.component.html',
  styleUrls: ['search-logo.component.css']
})
export class SearchLogoComponent implements OnChanges {
  @Input() isBig: boolean;

  toggleAnimation() {
    this.isBig = !this.isBig;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!changes.isBig.previousValue) {
      this.toggleAnimation();
    }
  }
}
