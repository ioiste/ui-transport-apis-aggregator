import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ApiError, TransportService } from '../shared/services/transport.service';
import { Subscription } from 'rxjs';
import { SearchResultModel } from '../shared/models/search-result.model';
import * as moment from 'moment';
import { environment } from '../../environments/environment';
import { now } from 'moment';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit, OnDestroy {
  isLoading = false;
  results: SearchResultModel[];
  error?: ApiError;
  transportSubscription: Subscription;
  minGoDepartureDate = moment(now()).format();
  minReturnDepartureDate = null;
  formSubmitted = false;
  requestDurationSeconds: number;
  isMobile: boolean;
  username: string;

  constructor(private transportService: TransportService) {
  }

  ngOnInit(): void {
    this.isMobile = this.getIsMobile();
    window.onresize = () => {
      this.isMobile = this.getIsMobile();
    };
  }

  ngOnDestroy(): void {
    this.transportSubscription.unsubscribe();
  }

  setMockedSearchParams(form: NgForm): void {
    if (!form.value.resultsLiveData) {
      form.controls.from.setValue('Iasi');
      form.controls.to.setValue('Bucuresti');
      form.controls.departure_date.setValue(moment('27-07-2020T07:00', environment.API_DATE_FORMAT));
      form.controls.one_way.setValue(false);
      form.controls.pax_adults.setValue(1);
      form.controls.pax_children.setValue(0);
      form.controls.pax_infants.setValue(0);
      form.controls.return_departure_date.setValue(moment('31-07-2020T12:00', environment.API_DATE_FORMAT));
      form.controls.searchFlights.setValue(true);
      form.controls.searchTrains.setValue(true);
      form.controls.searchBuses.setValue(true);
      form.controls.searchPublicTransport.setValue(true);
      form.controls.resultsLocaleRo.setValue(true);
      form.controls.resultsFromCache.setValue(true);
      form.form.disable({onlySelf: true});
      form.controls.resultsLiveData.enable({onlySelf: true});
      form.form.setErrors(null);
    } else {
      form.form.enable({onlySelf: true});
      this.setMinReturnDepartureDate(form);
    }
  }

  onSubmit(form: NgForm) {
    this.formSubmitted = true;
    if (!form.valid) {
      return;
    }
    this.results = null;
    this.error = null;
    this.isLoading = true;
    const startRequestTimer = moment(now());
    const goDepartureDate =
      moment(form.value.departure_date._d).format(environment.API_DATE_FORMAT);
    const returnDepartureDate = form.value.return_departure_date != null ?
      moment(form.value.return_departure_date._d).format(environment.API_DATE_FORMAT) : null;
    const filterTransportTypes = this.getFilteredTransportTypes(
      form.value.searchFlights,
      form.value.searchTrains,
      form.value.searchBuses,
      form.value.searchPublicTransport
    );
    const transportObs = this.transportService.search(
      form.value.from,
      form.value.to,
      goDepartureDate,
      form.value.pax_adults,
      form.value.pax_children,
      form.value.pax_infants,
      form.value.resultsLocaleRo,
      form.value.resultsFromCache,
      form.value.resultsLiveData,
      this.username,
      returnDepartureDate,
      filterTransportTypes,
    );
    this.transportSubscription = transportObs.subscribe(
      response => {
        this.setResults(response);
        this.isLoading = false;
      },
      error => {
        this.setError(error);
        this.isLoading = false;
      },
      () => {
        this.requestDurationSeconds =
          moment.duration(moment(now()).diff(startRequestTimer)).asSeconds();
      }
    );
  }

  getFilteredTransportTypes(
    searchFlights: boolean,
    searchTrains: boolean,
    searchBuses: boolean,
    searchPublicTransport: boolean
  ) {
    let filter = '';
    filter += !searchFlights ? ',aircraft' : '';
    filter += !searchTrains ? ',train' : '';
    filter += !searchBuses ? ',bus' : '';
    filter += !searchPublicTransport ? ',public_transport' : '';

    return filter !== '' ? filter.substr(1) : null;
  }

  setResults(results: SearchResultModel[]) {
    this.results = results;
  }

  setError(error: HttpErrorResponse) {
    this.error = new ApiError(
      error.error.type,
      error.error.title,
      error.error.detail,
      error.error.instance
    );
  }

  setMinReturnDepartureDate(form: NgForm) {
    this.minReturnDepartureDate = form.value.departure_date._d;
  }

  getMinReturnDepartureDate() {
    return this.minReturnDepartureDate != null ? this.minReturnDepartureDate : this.minGoDepartureDate;
  }

  getIsMobile(): boolean {
    const w = document.documentElement.clientWidth;
    const breakpoint = 1024;

    return w < breakpoint;
  }

  setUsername(username: string) {
    this.username = username;
  }
}
