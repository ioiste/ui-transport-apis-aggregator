import { Component, Input, OnInit } from '@angular/core';
import { SearchResultModel } from '../../shared/models/search-result.model';
import { ApiError } from '../../shared/services/transport.service';

@Component({
  selector: 'app-search-summary',
  templateUrl: './search-summary.component.html',
  styleUrls: ['./search-summary.component.css']
})
export class SearchSummaryComponent implements OnInit {
  @Input() results?: SearchResultModel[];
  @Input() error?: ApiError;
  @Input() requestDurationSeconds: number;

  constructor() {
  }

  ngOnInit(): void {
  }

  getSummaryStyle() {
    if (this.error != null) {
      return 'danger';
    }
    if (this.results.length === 0) {
      return 'warning';
    }

    return 'success';
  }

  getSummaryMessage() {
    if (this.error != null) {
      return 'Ceva nu a mers bine. Verifică datele introduse și încearcă iarăși. ' +
        'Detalii: <strong>' +
        (this.error.title.split('Transport:')[1] !== undefined ?
          this.error.title.split('Transport:')[1] :
          'N/A')
        + '</strong>.';
    }
    if (this.results.length === 0) {
      return 'Nu am găsit rezultate pentru datele introduse.';
    }

    return 'Am găsit <strong>' + this.results.length + ' rezultate </strong> în ' +
      '<strong>' + this.requestDurationSeconds + ' secunde </strong>.';
  }
}
