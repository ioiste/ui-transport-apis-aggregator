import { Component } from '@angular/core';
import { DateTimeAdapter } from '@danielmoncada/angular-datetime-picker';
import { registerLocaleData } from '@angular/common';
import localeRo from '@angular/common/locales/ro';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'taag-ui';

  constructor(dateTimeAdapter: DateTimeAdapter<any>) {
    registerLocaleData(localeRo, 'ro');
    dateTimeAdapter.setLocale('ro');
  }
}
