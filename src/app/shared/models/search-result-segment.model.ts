import { Deserializable } from './deserializable.model';

export class SearchResultSegmentModel implements Deserializable {
  public departureTimestamp: number;
  public departurePlace: string;
  public departureAirport?: string;
  public arrivalTimestamp: number;
  public arrivalPlace: string;
  public arrivalAirport?: string;
  public companyCode?: string;
  public company: string;
  public transportNo: string;
  public transportType: string;
  public deepLink?: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
