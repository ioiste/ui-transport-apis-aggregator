import { Deserializable } from './deserializable.model';
import { SearchResultSegmentModel } from './search-result-segment.model';

export class SearchResultModel implements Deserializable {
  public uniqueId: string;
  public segmentsGo: SearchResultSegmentModel[];
  public segmentsBack?: SearchResultSegmentModel[];
  public travelDuration: string;
  public departureTimestamp: number;
  public departurePlace: string;
  public departureCountry?: string;
  public arrivalTimestamp: number;
  public arrivalPlace: string;
  public arrivalCountry?: string;
  public price?: number;
  public currency?: string;
  public deepLink?: string;
  public dataInfo: { source: string, timestamp: number };

  deserialize(input: any): this {
    // Assign input to our object BEFORE deserialize our segments
    // to prevent already deserialized segments from being overwritten.
    Object.assign(this, input);

    // Iterate over all segments for our result and map them to a proper `Segment` model
    this.segmentsGo = input.segments.go.map(
      segmentGo => new SearchResultSegmentModel().deserialize(segmentGo)
    );
    this.segmentsBack = input.segments.back.map(
      segmentsBack => new SearchResultSegmentModel().deserialize(segmentsBack)
    );

    return this;
  }
}
