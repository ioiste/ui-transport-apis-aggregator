import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ImagesService {
  private imagesDir = '../../assets/images/';

  constructor() {
  }

  getCompanyImage(company: string, dataSource: string, transportType: string) {
    switch (dataSource) {
      case 'Kiwi-Transport':
        return 'https://images.kiwi.com/airlines/64x64/' + company + '.png';
      default:
        return this.getTransportTypeIcon(transportType, 'lg');
    }
  }

  getTransportTypeIcon(transportType: string, size: string) {
    let iconName = '';
    switch (transportType) {
      case 'aircraft':
        iconName = 'aircraft-transport-type-';
        break;
      case 'train':
      case 'regionalTrain':
        iconName = 'train-transport-type-';
        break;
      case 'bus':
      case 'busRapid':
        iconName = 'bus-transport-type-';
        break;
      case 'subway':
        iconName = 'subway-transport-type-';
        break;
      case 'lightRail':
        iconName = 'tram-transport-type-';
        break;
      default:
        return iconName;
    }
    return this.imagesDir + iconName + size + '.png';
  }

  getArrowIcon(direction: string) {
    return this.imagesDir + 'arrow-' + direction + '.png';
  }
}
