import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subject } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { SearchResultModel } from '../models/search-result.model';
import { EnvironmentService } from './environment.service';

export class ApiError {
  constructor(
    public type: string,
    public title: string,
    public detail?: string,
    public instance?: string
  ) {
  }
}

@Injectable({
  providedIn: 'root'
})
export class TransportService {
  searchResults = new Subject<SearchResultModel[]>();

  constructor(private http: HttpClient, private envService: EnvironmentService) {
  }

  search(
    from: string,
    to: string,
    departureDate: string,
    paxAdults: number,
    paxChildren: number,
    paxInfants: number,
    resultsLocaleRo: boolean,
    resultsFromCache: boolean,
    resultsLiveData: boolean,
    username: string,
    returnDepartureDate?: string,
    filterTransportTypes?: string,
  ) {
    return this.http.get<SearchResultModel[]>(
      '/api/transport?' +
      'from=' + from + '&' +
      'to=' + to + '&' +
      'goDepartureDt=' + departureDate + '&' +
      (returnDepartureDate ?
        'backDepartureDt=' + returnDepartureDate + '&' : '') +
      (filterTransportTypes ?
        'filterTransportTypes=' + filterTransportTypes + '&' : '') +
      (resultsLocaleRo ? 'locale=ro&' : '') +
      (!resultsFromCache ? 'disableCache=true&' : '') +
      (!resultsLiveData ? 'liveData=false&' : '') +
      'paxConfig=' + paxAdults + '_' + paxChildren + '_' + paxInfants + '&' +
      'username=' + username,
      {
        headers: new HttpHeaders({
          Authorization: 'Bearer web-app/' + this.envService.env.apiAuth
        })
      }
    ).pipe(
      map(results => {
        return Object.values(results).map(result => {
          return new SearchResultModel().deserialize(result);
        });
      }),
      tap(response => {
        this.searchResults.next(response);
      })
    );
  }
}
