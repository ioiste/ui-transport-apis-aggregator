import { EnvironmentModel } from '../models/environment.model';
import { Injectable } from '@angular/core';

@Injectable({providedIn: 'root'})
export class EnvironmentService {
  public env: EnvironmentModel;

  constructor() {
    this.env = new EnvironmentModel();
  }
}
