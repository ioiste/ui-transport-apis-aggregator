import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EnvironmentService } from './environment.service';
import { EnvironmentModel } from '../models/environment.model';

@Injectable({providedIn: 'root'})
export class EnvironmentHttpService {

  constructor(private http: HttpClient, private environmentService: EnvironmentService) {
  }

  initializeApp(): Promise<any> {
    return new Promise(
      (resolve) => {
        this.http.get('assets/env.json')
          .toPromise()
          .then(response => {
              this.environmentService.env = response as EnvironmentModel;
              resolve();
            }
          );
      }
    );
  }
}
