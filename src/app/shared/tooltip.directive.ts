import { Directive, ElementRef, Input, HostListener, OnDestroy } from '@angular/core';

declare var $: any;

@Directive({
  selector: '[appTooltip]'
})
export class TooltipDirective implements OnDestroy {
  @Input()
  public appTooltip: string;

  constructor(private elementRef: ElementRef) {
  }

  @HostListener('mouseenter')
  public onMouseEnter(): void {
    const nativeElement = this.elementRef.nativeElement;
    $(nativeElement).tooltip('show');
  }

  ngOnDestroy(): void {
    const nativeElement = this.elementRef.nativeElement;
    $(nativeElement).tooltip('dispose');
  }
}
